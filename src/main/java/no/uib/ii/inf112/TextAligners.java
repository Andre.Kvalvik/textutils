package no.uib.ii.inf112;

public class TextAligners implements TextAligner {

	@Override
	public String center(String text, int width) {
		int a = text.length();
		if (width < a) {
			throw new IllegalArgumentException("text cant be larger than width");
		}
		int b = width - a;
		if (!((b)%2 == 0)) {
			throw new IllegalArgumentException("can't be centered");
		}
		String space = " ".repeat(b/2);
		String result = (space + text + space);
		return result;
	}

	@Override
	public String flushRight(String text, int width) {
		int textsize = text.length();
		
		if (width < textsize) {
			throw new IllegalArgumentException("text cant be larger than width");
		}
		int numSpaces = width - textsize;
		String space = " ".repeat(numSpaces);
		String result = (space + text);
		
		return result;
	}

	@Override
	public String flushLeft(String text, int width) {
		int textsize = text.length();
		
		if (width < textsize) {
			throw new IllegalArgumentException("text cant be larger than width");
		}
		int numSpaces = width - textsize;
		String space = " ".repeat(numSpaces);
		String result = (text + space);
		
		return result;
	}

	@Override
	public String justify(String text, int width) {
		int textsize = text.length();
		String textResult = text;
		
		if (width < textsize) {
			throw new IllegalArgumentException("text cant be larger than width");
		}
		
		int numSpaces = width - textsize;
		while (numSpaces != 0) {
			for (int i = 0; i < textsize; i++) {
				char c = text.charAt(i);
				if (c == ' ') {
					String s1 = text.substring(0, i);
					String s2 = text.substring(i, textsize-1);
					textResult = (s1 + " " + s2);
					textsize++;
				}
			}
		}
		
		return textResult;
	}

}
